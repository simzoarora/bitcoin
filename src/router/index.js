import Vue from 'vue'
import VueRouter from 'vue-router'  

Vue.use(VueRouter);

import Home from '@/views/Home'

import Login from '@/modules/auth/views/Login'
import Register from '@/modules/auth/views/Register'
import PrivacyPolicy from '@/views/PrivacyPolicy' 
import TermsOfService from '@/views/TermsOfService'
import Imprint from '@/views/Imprint'
import About from '@/views/About'
import Contact from '@/views/Contact'

const routes = [
  {
    path: '/',  
    name: 'home',
    component: Home
  },
  {
    path: '/login',
    name: 'login',
    component: Login
  },
  {
    path: '/register',
    name: 'register',
    component: Register
  },
  {
    path: '/privacypolicy',
    name: 'privacypolicy',
    component: PrivacyPolicy
  },
  {
    path: '/termsofservice',
    name: 'termsofservice',
    component: TermsOfService
  },
  {
    path: '/imprint',
    name: 'imprint',
    component: Imprint
  },
 {
    path: '/about',
    name: 'about',
    component: About
  },
  {
    path: '/contact',
    name: 'contact',
    component: Contact
    },
]

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes
})

router.beforeEach((to, from, next) => {
  let loggedIn = !!localStorage.getItem('user')
  let publicsharekey = !!localStorage.getItem('publicsharekey')

  if (to.matched.some(record => record.meta.requiresAuth) && !loggedIn) {
    next('/')
  } else {

    if (publicsharekey && loggedIn ){
      
      let id = localStorage.getItem('publicsharekey'); 
      localStorage.removeItem('publicsharekey');
      next('/lists/'+id+'/duplicate');
    } else next()
  }
})

export default router
